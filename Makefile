
TARGET = clarus-bulk-upload-checker

SRC = $(TARGET).cpp

CC = g++

all: begin $(TARGET).exe

%.exe: $(SRC)
	@echo Compiling $@
	$(CC) $^ -o $@

begin:
	@echo
	@$(CC) --version
	@echo

