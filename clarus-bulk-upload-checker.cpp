/******************************************************************************
 *       Copyright (c) 2018, TZ Medical, Inc.
 *
 *       All rights reserved.
 *
 *       Redistribution and use in source and binary forms, with or without
 *       modification, are permitted provided that the following conditions
 *       are met:
 *
 *       Redistributions of the source code must retain the above copyright
 *       notice, this list of conditions, and the disclaimer below.
 *
 *       TZ Medical's name may not be used to endorse or promote products
 *       derived from this software without specific prio written permission.
 *
 *       DISCLAIMER:
 *       THIS SOFTWARE IS PROVIDED BY TZ MEDICAL "AS IS" AND ANY EXPRESS OR
 *       IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *       WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 *       NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT SHALL TZ MEDICAL BE
 *       LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *       CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *       SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *       BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *       WHETEHR IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *       OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *       EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/


//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <string>
#include <list>
#include <map>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>

#include "string.h"

using namespace std;


//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

/***   Feature Defines   ***/
#define CHECK_CRC             1
#define USE_MAP               0

#define EVENT_ID_STRING       "TZEVT"
#define ACTIONS_ID_STRING     "TZACT"
#define TZMR_DESC_STRING      "TZMR\0\0"
#define TZMR_FIRMWARE_VERSION (30)
#define H3R_DESC_STRING       "H3R  "
#define H3R_FIRMWARE_VERSION  (12)

#define MIN_TZE_SIZE          32      // Bytes
#define MAX_TZE_SIZE         256      // Bytes

// These defines manage how _many_ files can be requested when this program is
// run. We need to limit this to prevent DoS problems caused by devices with
// huge blocks of missing files.
#define MAX_SCP_FILES_PER_HOUR    (360) // This assumes a typical case of 10 seconds per file
#define CRON_JOB_PERIOD_HOURS     (2)   // This is a balance of server load versus responsiveness
#define MAX_REQUEST_FILE_COUNT    (MAX_SCP_FILES_PER_HOUR * CRON_JOB_PERIOD_HOURS)

#define MAX_TZA_LENGTH            (32768)


//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

typedef struct {
  uint32_t scp_start;
  uint32_t scp_count;
  uint32_t scp_end;
} scp_range_t;

#if USE_MAP == 1
typedef map<string, list<scp_range_t> * > scp_map_t;
typedef map<string, uint32_t> total_map_t;
#endif

/***   Possible TAG values for each entry   ***/
typedef enum {
   ACTIONS_REQUEST_SCP_BLOCK        = 20,
   ACTIONS_RETRANSMIT_SCP_FILE      = 21,
   ACTIONS_REQUEST_RECENT_SCP_BLOCK = 22,
   ACTIONS_REQUEST_TZR_FILE         = 30,
   ACTIONS_UPDATE_SETTINGS          = 40,
   ACTIONS_DISPLAY_MESSAGE          = 50,
   ACTIONS_TAG_REQUEST_ERROR_LOG    = 60,
   ACTIONS_TAG_UPDATE_FIRMWARE      = 100,
   ACTIONS_END_STUDY                = 200,
   ACTIONS_START_STUDY              = 201,
   ACTIONS_TAG_FORMAT_SD            = 220,

   BAD_TAG                          = 254,
   ACTIONS_TERMINATOR               = 255
} action_tag_t;



//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

static unsigned short const ccitt_crc16_table[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
  0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
  0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
  0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
  0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
  0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
  0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
  0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
  0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
  0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
  0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
  0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
  0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
  0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
  0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
  0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
  0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
  0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
  0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
  0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
  0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
  0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
  0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

#if USE_MAP == 1
static scp_map_t scp_range_map;
static scp_map_t scp_actions_map;
static total_map_t scp_total_map;
#else
static list<scp_range_t> scp_range_list;
static list<scp_range_t> scp_actions_list;
static string serial_number;
uint32_t total_requested = 0;
#endif


//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

static int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal);

static int process_event_files(string *inDir);
static int parse_event_file(string *fileName, scp_range_t *scp_range, string *p_serial);

static int add_to_scp_list(scp_range_t *scp_range, string *p_serial);
static int process_scp_list(string *inDir);

static int add_scp_request_action(uint32_t scp_start, uint8_t scp_count, string *p_serial);
static int generate_tza_file(string *filename, string *p_serial,
    list<scp_range_t> *p_list, uint16_t file_id);


//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
//    main()
//
//    This function processes each *.scp in the directory and outputs a
//    corresponding *.txt file with the ECG data for each valid file found.
//==============================================================================
int main(int argc, char *argv[])
{
   unsigned int searchNumber = 0;
   string inDir = "./";

   // Silently discard clog outputs
   clog.rdbuf(NULL);

   if(argc >= 2){
      inDir.assign(argv[1]);
      if( ('/' != inDir[inDir.length()]) && ('\\' != inDir[inDir.length()]) )
      {
        inDir.push_back('/');
      }
   }

   if(process_event_files(&inDir)){
      cerr << "ERROR: Aborting!" << endl;
      return -1;
   }

   process_scp_list(&inDir);

   return 0;
}



//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//==============================================================================
int crcBlock(unsigned char *data, unsigned int length, unsigned short *crcVal)
{
   unsigned int j;
   for(j = 0; j < length; j++){
      *crcVal = ccitt_crc16_table[(data[j] ^ (*crcVal>>8)) & 0xff] ^ (*crcVal<<8);
   }
   return 0;
}


//==============================================================================
int parse_event_file(string *fileName, scp_range_t *scp_range, string *p_serial)
{
   unsigned short ourcrcValue;                  // The CRC we calculate
   unsigned short theircrcValue;                // The CRC from the file
   unsigned int length;                         // The length read from the file

   unsigned char * pRead;                       // Pointer used for the read command
   unsigned short *shortCaster;
   unsigned int *intCaster;

   unsigned char firstBlock[16] = {0};
   
   unsigned int i;

   ourcrcValue = 0xffff;                        // CRC is based off an initial value of 0xffff

   scp_range->scp_start = 0;
   scp_range->scp_count = 0;
   scp_range->scp_end = 0;

   // Open the file and check the file length
   fstream inFile (fileName->c_str(), ios::in | ios::binary);
   inFile.seekg(0, ios::end);
   unsigned int fileLength = inFile.tellg();
   inFile.seekg(0, ios::beg);

   // Make sure the file is not too big for the format
   if(fileLength > MAX_TZE_SIZE){
      cerr << *fileName << ": File Size is SIGNIFICANTLY larger than expected (" 
           << fileLength << "). Skipping." << endl;
      return -1;
   }
   else if(fileLength < MIN_TZE_SIZE){
      cerr << *fileName << ": File size is smaller that expected ("
           << fileLength << "). Skipping." << endl;
      return -1;
   }

   // Read in the first 16-byte block to see if the file has been encrypted
   inFile.read((char *) firstBlock, 16);           // Read the first block into RAM

   // Check for the format identifier string
   string str;
   str.assign((const char *) &firstBlock[6]);   // Copy what should be the "TZEVT" string
   
   if(str.compare(0, 6, EVENT_ID_STRING))
   {
      cerr << *fileName << ": Invalid file. Skipping." << endl;
     return -1;
   }
   else{

      i = 0;
      shortCaster = (unsigned short *) &firstBlock[i];
      theircrcValue = *shortCaster;
      i += 2;

      intCaster = (unsigned int *) &firstBlock[i];
      length = *intCaster;
      i += 4;

      pRead = new unsigned char [fileLength];          // Allocate enough space to read in the whole file     
      for(i = 0; i < 16; i++){
         pRead[i] = firstBlock[i];                 // Copy the first block into the file buffer
      }
      inFile.read((char *) &pRead[16], fileLength-16);    // Store the remainder of the file in memory

      crcBlock(&pRead[2], length-2, &ourcrcValue); // Calculate the CRC for the remainder of the file
   }

#if CHECK_CRC == 1
   if(ourcrcValue != theircrcValue)
   {
      cerr << *fileName << ": Invalid CRC. Skipping." << endl;
      return -1;
   }
#endif

   i = 6;
   //clog << "File Length: " << length << endl;            // Print out the file length 

   //clog << "Format Identifier String: " << &pRead[i] << endl;
   i += 6;
   string dev_id((const char *) &pRead[i]);
   //clog << "Device Identifier String: " << &pRead[i] << endl;
   i += 6;

   unsigned int firmwareVersion = pRead[i];              // Firmware version (10 == 1.0)
   //clog << "Firmware Version: " << firmwareVersion/10 
   //     << "." << firmwareVersion%10 << endl;
   i += 1;

   //clog << "Serial Number: " << &pRead[i] << endl;       // Parse out the Serial Number String
#if USE_MAP == 0
   if(!serial_number.empty() && (string::npos == serial_number.compare((const char *) &pRead[i])) )
   {
     cerr << *fileName << ": Serial Number Changed! Aborting." << endl;
     return -2;
   }
#endif
   if(string::npos != dev_id.find("TZMR"))
   {
     p_serial->assign((const char *) &pRead[i], 8);
     string::iterator it=p_serial->end();
     while('\0' == *(--it))
     {
       p_serial->erase(it);
     }
     i += 8;
   }
   else if(string::npos != dev_id.find("H3R"))
   {
     p_serial->assign((const char *) &pRead[i], 11);
     string::iterator it=p_serial->end();
     while('\0' == *(--it))
     {
       p_serial->erase(it);
     }
     i += 11;
   }

   //clog << "Patient ID: " << &pRead[i] << endl;          // Parse out the Patient ID
   i += 40;


   //unsigned int tag = pRead[i];                                         // Identifier tag
   i += 1;
   //unsigned int sequenceNumber = pRead[i] + ((int)pRead[i+1] << 8)
   //               + ((int)pRead[i+2] << 16) + ((int)pRead[i+3] << 24);  // Sequence number
   i += 4;
   //unsigned int sampleCount = pRead[i] + ((short)pRead[i+1] << 8);      // Sample count
   i += 2;
   //unsigned int year = pRead[i] + ((short)pRead[i+1] << 8);             // Year
   i += 2;
   //unsigned int month = pRead[i];                                       // Month
   i += 1;
   //unsigned int day = pRead[i];                                         // Day
   i += 1;
   //unsigned int hour = pRead[i];                                        // Hour
   i += 1;
   //unsigned int minute = pRead[i];                                      // Minute
   i += 1;
   //unsigned int second = pRead[i];                                      // Seconds
   i += 1;
   //unsigned int milliseconds = pRead[i] * 4;                            // Milliseconds / 4
   i += 1;
   //signed short timeZone = pRead[i] + ((short)pRead[i+1] << 8);           // Time zone (minutes from UTC)
   i += 2;
   unsigned int dataLength = pRead[i];                                  // Data Length
   i += 1;
   //unsigned int data = 0, k, mult = 1;
   //for(k = 0; k < dataLength; k++){
   //   data = data + pRead[i+k]*mult;                                    // Data Value
   //   mult *= 256;
   //}
   i += dataLength;
   unsigned int fCount = pRead[i];                                      // Number of files
   i += 1;
   unsigned int fStart =  pRead[i] + ((int)pRead[i+1] << 8)             // First file sent
                  + ((int)pRead[i+2] << 16) + ((int)pRead[i+3] << 24);
   i += 4;

   if(fCount)
   {
     scp_range->scp_start = fStart;
     scp_range->scp_count = fCount;
     scp_range->scp_end = fStart + fCount;
   }

   delete[] pRead;                  // Free the buffer we used for parsing

   return 0;
}

//==============================================================================
int add_to_scp_list(scp_range_t *scp_range, string *p_serial)
{
  list<scp_range_t>::iterator current;
  uint8_t match_found = 0;
  list<scp_range_t> *p_scp_range;

#if USE_MAP == 1
  scp_map_t::iterator it = scp_range_map.find(*p_serial);

  if(it != scp_range_map.end())
  {
    p_scp_range = it->second;
  }
  else
  {
    p_scp_range = new list<scp_range_t>;
    scp_range_map.insert(it, scp_map_t::value_type(*p_serial, p_scp_range));
  }
#else
  p_scp_range = &scp_range_list;
#endif

  for(current = p_scp_range->begin(); current != p_scp_range->end(); current++)
  {
    // Merge the entries if they overlap
    if( (current->scp_start < scp_range->scp_start) 
        && (current->scp_end >= scp_range->scp_start) )
    {
      if(current->scp_end < scp_range->scp_end)
      {
        clog << *p_serial << ": 1) Merging [" << current->scp_start << ":" << current->scp_end << "]";
        clog << " and [" << scp_range->scp_start << ":" << scp_range->scp_end << "]" << endl;
        current->scp_end = scp_range->scp_end;

        // Now, we need to see if we overlap the _next_ entry on the list
        list<scp_range_t>::iterator next = current;
        while(++next != p_scp_range->end())
        {
          if(next->scp_start < current->scp_end)
          {
            clog << *p_serial << ": 5) Merging [" << current->scp_start << ":" << current->scp_end << "]";
            clog << " and [" << next->scp_start << ":" << next->scp_end << "]" << endl;
            if(next->scp_end > current->scp_end) current->scp_end = next->scp_end;
            p_scp_range->erase(next);
          }
          next = current;
        }
      }
      match_found = 1;
      break;
    }
    else if( (current->scp_start >= scp_range->scp_start)
        && (current->scp_end <= scp_range->scp_end) )
    {
        clog << *p_serial << ": 6) Merging [" << current->scp_start << ":" << current->scp_end << "]";
        clog << " and [" << scp_range->scp_start << ":" << scp_range->scp_end << "]" << endl;
        current->scp_start = scp_range->scp_start;
        current->scp_end = scp_range->scp_end;

        // Now, we need to see if we overlap the _next_ entry on the list
        list<scp_range_t>::iterator next = current;
        while(++next != p_scp_range->end())
        {
          if(next->scp_start < current->scp_end)
          {
            clog << *p_serial << ": 5) Merging [" << current->scp_start << ":" << current->scp_end << "]";
            clog << " and [" << next->scp_start << ":" << next->scp_end << "]" << endl;
            if(next->scp_end > current->scp_end) current->scp_end = next->scp_end;
            p_scp_range->erase(next);
          }
          next = current;
        }
        match_found = 1;
        break;
    }
    else if( (current->scp_start < scp_range->scp_end)
        && (current->scp_end >= scp_range->scp_end) )
    {
      if(current->scp_start > scp_range->scp_start)
      {
        clog << *p_serial << ": 2) Merging [" << scp_range->scp_start << ":" << scp_range->scp_end << "]";
        clog << " and [" << current->scp_start << ":" << current->scp_end << "]" << endl;
        current->scp_start = scp_range->scp_start;
      }
      match_found = 1;
      break;
    }
    // Insert before this entry if we've gone past
    else if( current->scp_start > scp_range->scp_end )
    {
      clog << *p_serial << ": 3) Inserting [" << scp_range->scp_start << ":" << scp_range->scp_end << "]";
      clog << " before [" << current->scp_start << ":" << current->scp_end << "]" << endl;
      p_scp_range->insert(current, *scp_range);
      match_found = 1;
      break;
    }
  }

  if(!match_found)
  {
    clog << *p_serial << ": 4) Appending [" << scp_range->scp_start << ":" << scp_range->scp_end << "]" << endl;
    p_scp_range->push_back(*scp_range);
  }

  return 0;
}

//==============================================================================
int process_scp_list(string *inDir)
{
  list<scp_range_t>::iterator current;
  
  // Generate a random number for the file_id
  srand(time(NULL));
  uint16_t file_id = (rand() % 63000) + 2000;
  cout << "--------------------------" << endl;
  cout << "Using random file_id " << file_id << endl;
  cout << "--------------------------" << endl;
  cout << endl;

  uint32_t *p_total;

#if USE_MAP == 1
  scp_map_t::iterator it;
  for(it = scp_range_map.begin(); it != scp_range_map.end(); it++)
  {
    list<scp_range_t> *p_scp_range = it->second;
    string serial_number = it->first;

    total_map_t::iterator total_it = scp_total_map.find(serial_number);
    if(total_it == scp_total_map.end())
    {
      total_it = scp_total_map.insert(total_it, total_map_t::value_type(serial_number, 0));
    }
    p_total = &total_it->second;
#else
    list<scp_range_t> *p_scp_range = &scp_range_list;
    
    p_total = &total_requested;
#endif

    for(current = p_scp_range->begin(); current != p_scp_range->end(); current++)
    {
      clog << "SCP List: " << current->scp_start << " - " << current->scp_end - 1 << endl;
      list<scp_range_t>::iterator next = current;
      if(++next != p_scp_range->end())
      {
        if(next->scp_start > current->scp_end)
        {
          uint32_t scp_start = current->scp_end;
          uint32_t scp_count = next->scp_start - current->scp_end;

          cout << serial_number << ": ";
          cout << "GAP - " << scp_start << " to " << next->scp_start - 1 
            << " (files: " << scp_count <<  ")" << endl;


          while(scp_count && (*p_total < MAX_REQUEST_FILE_COUNT) )
          {
            uint8_t request_count;
            if(scp_count > 250) request_count = 250;
            else request_count = scp_count;
            add_scp_request_action(scp_start, request_count, &serial_number);

            scp_count -= request_count;
            scp_start += request_count;
            *p_total += request_count;
          }
        }
      }
    }
    
#if USE_MAP == 1
    scp_map_t::iterator actions_it = scp_actions_map.find(serial_number);
    if(actions_it != scp_actions_map.end())
    {
      list<scp_range_t> *p_scp_actions = actions_it->second;
#else
    if(!scp_actions_list.empty())
    {
      list<scp_range_t> *p_scp_actions = &scp_actions_list;
#endif
      string filename = *inDir;
      filename.append(serial_number);
      filename.append(".tza");
      generate_tza_file(&filename, &serial_number, p_scp_actions, file_id);
    }
    else
    {
      cout << serial_number << ": No missing files!" << endl;
    }

    cout << endl;

#if USE_MAP == 1
    delete p_scp_range;
  }
#endif


  return 0;
}

//==============================================================================
int process_event_files(string *inDir)
{
   DIR *pDir = NULL;
   struct dirent *direntry = NULL;
   struct stat entrystat;
   size_t found;
   char buffer[16];
   unsigned int length, offset = 0;
   bool retVal = 0;

   if(inDir->find("/.") == string::npos){
      pDir = opendir(inDir->c_str());

      if(pDir == NULL){
         cout << "ERROR: Could not open Directory" << endl;
         return 0;
      }

      while(!retVal && (direntry = readdir(pDir))){
         if(direntry == NULL){
            cout << "ERROR: Could not read directory entry" << endl;
            continue;
         }

         string fileName;
         fileName.assign(*inDir);
         fileName.append((const char *)direntry->d_name);

         if(stat(fileName.c_str(), &entrystat)){
            cout << "ERROR: Could not get entry stats for entry: " << direntry->d_name << endl;
            continue;
         }

         if(S_ISDIR(entrystat.st_mode)){
            fileName.append("/");
            int ret_val = process_event_files(&fileName);
            if(ret_val < 0) return ret_val;
         }
         else{
            found = fileName.find(".tze", 1);
            scp_range_t scp_range;
#if USE_MAP == 1
            string serial_number;
#endif
            if(found != string::npos){
               int ret_val = parse_event_file(&fileName, &scp_range, &serial_number);
               if(-2 == ret_val) return ret_val;
               if(scp_range.scp_count)
               {
                 add_to_scp_list(&scp_range, &serial_number);
                 clog << serial_number << ": SCP Files - " << scp_range.scp_start << " to " << scp_range.scp_end-1 << endl;
               }
            }
            else
            {
              found = fileName.find(".TZE", 1);
              if(found != string::npos){
                int ret_val = parse_event_file(&fileName, &scp_range, &serial_number);
                if(-2 == ret_val) return ret_val;
                if(scp_range.scp_count)
                {
                  add_to_scp_list(&scp_range, &serial_number);
                  clog << serial_number << ": SCP Files - " << scp_range.scp_start << " to " << scp_range.scp_end-1 << endl;
                }
              }
            }
         }
      }

      closedir(pDir);
   }

   return retVal;
}


//==============================================================================
static int add_scp_request_action(uint32_t scp_start, uint8_t scp_count, string *p_serial)
{
  scp_range_t new_request;
  list<scp_range_t> *p_scp_actions;

#if USE_MAP == 1
  scp_map_t::iterator it = scp_actions_map.find(*p_serial);

  if(it != scp_actions_map.end())
  {
    p_scp_actions = it->second;
  }
  else
  {
    p_scp_actions = new list<scp_range_t>;
    scp_actions_map.insert(it, scp_map_t::value_type(*p_serial, p_scp_actions));
  }
#else
  p_scp_actions = &scp_actions_list;
#endif
      
  new_request.scp_start = scp_start;
  new_request.scp_count = scp_count;
  new_request.scp_end = scp_start + scp_count;

  p_scp_actions->push_back(new_request);

  return 0;
}

//==============================================================================
static int generate_tza_file(string *filename, string *p_serial, 
    list<scp_range_t> *p_list, uint16_t file_id)
{
  list<scp_range_t>::iterator current;

  cout << *p_serial << ": Outputting to file - \"" << filename->c_str() << "\"" << endl;
  
  uint32_t i = 6;    // The first 6 bytes are length and CRC
  uint32_t j;
  uint32_t k;
  unsigned char buffer[MAX_TZA_LENGTH];

  memset(buffer, 0, sizeof(buffer));

  strncpy((char *) &buffer[i], ACTIONS_ID_STRING, 6);  // The format identifier string
  i += 6;
  
  if(7 == p_serial->length())
  {
    strncpy((char *) &buffer[i], TZMR_DESC_STRING, 6);   // The device ID string
    i += 6;

    buffer[i++] = TZMR_FIRMWARE_VERSION;             // This byte is F. Vers. (*10)

    strncpy((char *) &buffer[i], p_serial->c_str(), 8);      // The next 8 bytes are the serial number
    i += 8;                                // Increment the pointer
  }
  else
  {
    strncpy((char *) &buffer[i], H3R_DESC_STRING, 6);   // The device ID string
    i += 6;

    buffer[i++] = H3R_FIRMWARE_VERSION;             // This byte is F. Vers. (*10)

    strncpy((char *) &buffer[i], p_serial->c_str(), 11);      // The next 11 bytes are the serial number
    i += 11;                                // Increment the pointer
  }

  unsigned char *caster = (unsigned char *) &file_id;

  buffer[i++] = caster[0];
  buffer[i++] = caster[1];

  // SCP_REQUEST - request a block of 1 to 255 SCP files per request
  for(current = p_list->begin(); current != p_list->end(); current++)
  {
    cout << *p_serial << ": ";
    buffer[i++] = ACTIONS_REQUEST_SCP_BLOCK;
    buffer[i++] = 5;
    uint32_t temp = current->scp_start;
    cout << "Requesting " << current->scp_start;
    for(k = 0; k < 4; k++){
      buffer[i++] = temp & 0xff;
      temp = temp >> 8;
    }
    cout << " - " << current->scp_start + current->scp_count - 1 << endl;
    buffer[i++] = current->scp_count;
    buffer[i++] = '\0';
  }

  // Always end the file with the terminator
  buffer[i++] = ACTIONS_TERMINATOR;
  buffer[i++] = 1;
  buffer[i++] = 0xff;
  buffer[i++] = '\0';


  uint32_t length = i;
  i = 2;
    uint32_t temp = length;
  for(j = 0; j < 4; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  uint16_t crcValue = 0xffff;

  crcBlock(&buffer[2], length - 2, &crcValue);

  i = 0;
  temp = crcValue;
  for(j = 0; j < 2; j++){
    buffer[i++] = temp & 0xff;
    temp = temp >> 8;
  }

  fstream out_file(filename->c_str(), ios::out | ios::binary | ios::trunc);
  out_file.write((const char *) buffer, length);
  out_file.close();

  return 0;
}

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------






